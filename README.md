# Typescript test project

Test project with:

* **Language:** Typescript
* **Package Manager:** yarn

## How to use

Please see the [usage documentation](https://gitlab.com/gitlab-org/security-products/tests/common#how-to-use) for Security Products test projects.

## Supported Security Products Features

| Feature             | Supported                 |
|---------------------|---------------------------|
| SAST                | :white_check_mark: |
| Dependency Scanning | :x: |
| Container Scanning  | :x: |
| DAST                | :x: |

